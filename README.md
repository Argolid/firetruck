# FireTruck 🚒

Access the CurseForge API reliably and easily with FireTruck!

### API Usage

#### Get Addon by ID

##### `/v1/addon/{addonID}`

```ruby
GET https://firetruck.monster/v1/addon/310806

{
  "id": 310806,
  "slug": "watermark",
  "name": "Watermark",
  "summary": "Shows watermark on top centre screen",
  "authors": [
    {
      "name": "Gaz_",
      "id": 6881422
    }
  ],
  "icon": "https://media.forgecdn.net/avatars/thumbnails/185/18/256/256/636824897396091696.png",
  "categories": [
    "Map and Information",
    "Cosmetic"
  ],
  "game": "Minecraft"
}
```

#### Download File by ID and Addon ID

##### `/v1/addon/{addonID}/file/{fileID}/download`

```ruby
GET https://firetruck.monster/v1/addon/296062/file/2724357/download

REDIRECT to "https://media.forgecdn.net/files/2724/357/SkyFactory4-4.0.8.zip"
```



### Reliability

Curseforge has stated they will gate off their API eventually. When the time comes, this project will shift to using other means such as CurseForge's website to continue providing dependable data.
