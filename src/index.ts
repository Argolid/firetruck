import { Router } from 'worktop';
import { listen } from 'worktop/cache';

const API = new Router();

API.add('GET', '/v1/addon/:id', async (req, res) => {
  try {
    const addon = await (await fetch(`https://addons-ecs.forgesvc.net/api/v2/addon/${req.params.id}`)).json() as any;
    res.send(200, {
      id: addon.id,
      slug: addon.slug,
      name: addon.name,
      summary: addon.summary,
      authors: addon.authors.map((it: { name: any; userId: any; }) => ({ name: it.name, id: it.userId })),
      icon: addon.attachments[0].thumbnailUrl,
      categories: addon.categories.map((it: { name: any; }) => it.name),
    });
  } catch {
    res.send(500);
  }
});

API.add('GET', '/v1/addon/:id/file/:file/download', async (req, res) => {
  try {
    const url = await (await fetch(`https://addons-ecs.forgesvc.net/api/v2/addon/${req.params.id}/file/${req.params.file}/download-url`)).text();
    res.setHeader('Location', url)
    res.send(307);
  } catch {
    res.send(500);
  }
});

API.add('GET', '/', (req, res) => {
  res.setHeader('Location', 'https://gitlab.com/Argolid/firetruck')
  res.send(307);
});

listen(API.run);